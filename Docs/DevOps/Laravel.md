# Laravel

## Table of contents
- [CORS](#cors)

## Cors
In Laravel CORS is handled by Laravel itself and you can find the CORS config in `config/cors.php`.

You may need to set CORS for public paths like `storage`, `assets, etc. For this purpose you need to specify CORS at the webserver level.
#### Acpahe:
Specify CORS for your directory or file type in `.htaccess` file:
```apache
<IfModule mod_headers.c>
    <FilesMatch "\.(png|eot|otf|tt[cf]|woff2?)$">
        Header set Access-Control-Allow-Origin "*"
    </FilesMatch>
</IfModule>
```

#### OpenLiteSpeed:
OLS **doesn't support** header modification in `.htaccess`, you need to set header options in your vhost config, in the below example we want to set CORS for the storage link in public folder:
```
context /storage {
  location                $VH_ROOT/public_html/current/public/storage
  allowBrowse             1
  extraHeaders            Access-Control-Allow-Origin *

  rewrite  {

  }
  addDefaultCharset       off

  phpIniOverride  {

  }
}
```
Also you can create that from web admin panel using 7080 port.