# CyberPanel

## Installation

## Upgrade

## Reverse proxy

1. Firstly create a new website in CyberPanel, for reverse proxies your php version does not matter so choose any
2. after creating a website go to the website list and find your website and open the management page for it
3. scroll down until you reach the `vHost Conf` option and append the following text to it:

```conf
extprocessor SERVER_NAME {
  type                    proxy
  address                 SERVER_ADDRESS_AND_PORT
  maxConns                10
  initTimeout             600
  retryTimeout            0
  respBuffer              0
}

context / {
  type                    proxy
  handler                 SERVER_NAME
  addDefaultCharset       off
}
```

replace SERVER_ADDRESS_AND_PORT with your server and port in a `serveraddress:port` format  
server address can be localhost  
SERVER_NAME can be whatever you want, just make sure it's the same in both places
