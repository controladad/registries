# ELK Stack

***Note: this guide does not go over dashboard setup or internal tls setup***
## Log server installation and setup

The log server is the server that receives logs from other servers and displays them in the kibana interface

1. clone [https://github.com/deviantony/docker-elk](https://github.com/deviantony/docker-elk) (or fork as a base for custom changes)
2. make desired changes to the .env file
3. run using `docker-compose up -d`
4. open kibana on port 5601 (or configure a [reverse proxy](CyberPanel.md#Reverse%20Proxy) on port 5601) then login (default user is `elastic` and password is what you set in .env)

## Beats

Beats redirect data (metrics, logs, etc) from your production server to your main log server  
Different types of beats exist for different purposes, see a list [here](https://www.elastic.co/beats/)  
as an example, here's how to install [Filebeat](https://www.elastic.co/guide/en/beats/filebeat/current/index.html)

1. install filebeat according to your OS and such
2. open the configuration on `/etc/filebeat/filebeat.yml`
3. add an input stream from a file of your own choosing
4. under `setup.logstash` add your logstash instance to the host list

you should now see your log stream in kibana under `Observability->logs`

you can now configure filters from {docker-elk directory}/logstash/pipeline/logstash.conf  
if you have multiple beats that need to be filtered differently make a new pipeline file and add a `beats` type on a different port, make sure to open your port in docker-compose
