FROM odoo:16.0

RUN pip install jdate

RUN wget https://www.python.org/ftp/python/3.11.2/Python-3.11.2.tar.xz

RUN tar -xvf Python-3.11.2.tar.xz

RUN cd Python-3.11.2

RUN ./configure --enable-optimizations

RUN sudo make altinstall

RUN sudo update-alternatives --install /usr/bin/python3 python3 /usr/local/bin/python3.11.2